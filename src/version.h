/* Implement the version function which show the current version of spiek
 * 
 * Copyright (C) 2019  Peter van Gemert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

int func_version() {
    printf("Spiek %s\n",VERSION);

    printf("Copyright (C) 2022 Peter van Gemert\n");
    printf("License GPLv3+: GNU GPL version 3 ");
    printf("or later <https://gnu.org/licenses/gpl.html>.\n");
    printf("This is free software: you are free to change and ");
    printf("redistribute it.\n");
    printf("There is NO WARRANTY, to the extent permitted by law.\n");
    printf("\n");
    printf("GIVE CREDITS WHERE CREDITS ARE DUE\n");
    printf("\n");
    printf("The inspiration for spiek came after I found the cheat command on\n");
    printf("https://github.com/cheat/cheat. Cheat is written in python and I\n");
    printf("thought it would be a nice learning experience to rewrite it in C.\n");
    printf("\n");
    printf("Written by Peter van Gemert.\n\n");

    exit(0);
}
