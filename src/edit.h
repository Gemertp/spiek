/* Edit a new or existing spiekbrief.
 * 
 * Copyright (C) 2019  Peter van Gemert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

int func_edit(char spiekname[]) {

    // Check if the argument for -e is given. If not present,
    // then quit the program;
    if ( opt_e_arg[0] == '\0' ) {
        printf("opt_e_arg is empty\n");
    }

    // Get users home directory
    const char *homedir;

    if ((homedir = getenv("HOME")) == NULL) {
        homedir = getpwuid(getuid())->pw_dir;
    }


    /* We create a full path for the spiekbrief that
     * needs to be edited. We do not check if the spiekbrief
     * actually exists because with vi we create the
     * spiekbrief when it doesn't. If the spiekbrief does
     * exist, we open it in vi so it can be edited.
     */
    char spiekbriefh[100] = "";
    snprintf(   spiekbriefh, \
                sizeof(spiekbriefh), \
                "%s%s%s", \
                homedir, \
                "/.spiek/", \
                opt_e_arg);

    /* We run vi in a subprocess created with fork/execl
     */
    int pid;
    int status, ret;
    pid = fork();
    if ( pid == 0 ) {
        execl(VI,VI,spiekbriefh,(char *) NULL);
        printf("Error: could not start vi :");
        perror(spiekbriefh);
        exit(EXIT_FAILURE);
    }
    if (( ret = waitpid( pid, &status, 0)) == -1 ) {
        printf("parent error\n");
    }
}

// eof
