/* Implement the help function
 *
 * Copyright (C) 2019  Peter van Gemert
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

int func_help() {
    printf("Usage: spiek [-h|-l|v]\n");
    printf("       spiek [-e SPIEKBRIEF]\n");
    printf("       spiek [-p SPIEKBRIEF]\n");
    printf("Manage spiekbriefjes.\n");
    printf("\n");
    printf("Options\n");
    printf("  -e,  --edit [SPIEKBRIEF]]  Edit or create a spiekbrief.\n");
    printf("                             Only spiefbrief in ~/.spiek can\n");
    printf("                             be edited. New spiekbrief will be\n");
    printf("                             stored in ~/.spiek.\n");
    printf("  -h,  --help                Display this help message and exit.\n");
    printf("  -l,  --list                List all available spiekbriefjes.\n");
    printf("  -p,  --page [SPIEKBRIEF]   Display spiekbrief per page (less).\n");
    printf("  -v,  --version             Display version and exit.\n");
    printf("\n");
    printf("spiek version %s,  Copyright (C) 2019 Peter van Gemert\n",VERSION);
}

// eof

