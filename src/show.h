/* Show requested spiekbrief on STDOUT
 * 
 * Copyright (C) 2019  Peter van Gemert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

int func_show(char spiekname[], int pager) {

    if ( spiekname[0] == '\0' ) {
        exit(EXIT_FAILURE);
    }

    // Get users home directory
    const char *homedir;

    if ((homedir = getenv("HOME")) == NULL) {
        homedir = getpwuid(getuid())->pw_dir;
    }

    /* We have to check first if the given spiekname
     * does actually exist. There are two locations where
     * a spiekbrief may reside: /usr/share/spiek and ~/.spiek
     */
    char spiekbrief[100] = "";
    char spiekbriefh[100] = "";
    char spiekbriefd[100] = "";
    char spiekbriefu[100] = "";

    snprintf(   spiekbriefh, \
                sizeof(spiekbriefh), \
                "%s%s%s", \
                homedir, \
                "/.spiek/", \
                spiekname );

    snprintf(	spiekbriefd, \
		sizeof(spiekbriefd), \
		"%s%s%s", \
		homedir, \
		"/Dropbox/spiek/", \
		spiekname );

    snprintf(	spiekbriefu, \
		sizeof(spiekbriefu), \
		"%s%s", \
		"/usr/share/spiek/", \
		spiekname );


    FILE *fp;

    /* Scan all directories for spiekbrief
     * Last match will be shown
     */
    fp = fopen( spiekbriefu, "r" );
    if ( fp != NULL ) {
        fclose( fp );
        strcpy(spiekbrief,spiekbriefu);
    }

    fp = fopen( spiekbriefd, "r" );
    if ( fp != NULL ) {
        fclose( fp );
        strcpy(spiekbrief,spiekbriefd);
    }

    fp = fopen( spiekbriefh , "r" );
    if ( fp != NULL ) {
        fclose( fp );
        strcpy(spiekbrief,spiekbriefh);
    }

    // Do a cat or less on the spiekbrief
    if ( pager ) {

       // Code for using less to show the spiekbrief
       execl("/usr/bin/less","/usr/bin/less",spiekbrief,(char *) NULL);

    } else {

       execl("/usr/bin/cat","/usr/bin/cat",spiekbrief,(char *) NULL);
    }

}

// eof
