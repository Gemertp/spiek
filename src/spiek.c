/* Create, read and manage spiekbriefjes.
 * 
 * Copyright (C) 2019  Peter van Gemert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

char opt_e_arg[100] = "";
char opt_p_arg[100] = "";
int opt_given = 0;

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <getopt.h> 
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <sys/wait.h>
#include "constants.h"
#include "constants-options.h"
#include "help.h"
#include "options.h"
#include "version.h"
#include "list.h"
#include "show.h"
#include "edit.h"

int main(int argc, char **argv) {

    // Variables for files
    char ch, file_name[100];
    FILE *fp;

    opt_given = proc_options(argc, argv);

    if ( argc == 1 ) {
        func_help();
        exit(EXIT_FAILURE);
    }

    if ( opt_given == -1 ) {
        func_help();
        exit(EXIT_FAILURE);
    }

    if ( opt_given & opt_l ) {
        func_list();
        exit(EXIT_SUCCESS);
    }

    if ( opt_given & opt_e ) {
        func_edit(argv[1]);
        exit(EXIT_SUCCESS);
    }

    if ( opt_given & opt_h ) {
        func_help();
        exit(EXIT_SUCCESS);
    }

    if ( opt_given & opt_v ) {
        func_version();
        exit(EXIT_SUCCESS);
    }

    int pager=0;
    if ( opt_given & opt_p ) {
        pager=1;
    }
    

    /* When we arrive here we have processed all options. 
     * In func_show we will check if arg1 contains a real 
     * spiekbrief name and then show the contents.
     */
    char spiekbriefje[100] = "";
    if ( pager ) {
        strcpy(spiekbriefje,opt_p_arg);
    } else {
        strcpy(spiekbriefje,argv[1]);
    }
        
    func_show(spiekbriefje, pager);

    exit(EXIT_SUCCESS);
}

// eof
