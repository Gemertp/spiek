/* Process cli options given to spiek.
 * 
 * Copyright (C) 2019  Peter van Gemert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

int proc_options(int argc, char **argv) {
    // Variables for cli options
    int opt_count = 0;
    int opt_index = 0;
    int opt_given = 0; // Value of given cli options
    char *opt_string = ":lvhe:p:"; // List of possible options
                              // used by getopt_long

    // Corresponding long options fpt getopt_long()
    static struct option opt_long[] =
    {
        {"edit",    required_argument,  NULL,'e'},
        {"help",    no_argument,        NULL,'h'},
        {"list",    no_argument,        NULL,'l'},
        {"page",    required_argument,  NULL,'p'},
        {"version", no_argument,        NULL,'v'},
        {0,0,0,0}
    };

    // Process options
    while (( opt_index = getopt_long(argc, argv, opt_string, opt_long, &opt_index)) != -1 ) {
        switch (opt_index) {

            case 'e':   opt_given += opt_e;
                        strcpy(opt_e_arg,optarg);
                        opt_count++;
                        break;
            case 'h':   opt_given += opt_h;
                        opt_count++;
                        break;
            case 'l':   opt_given += opt_l;
                        opt_count++;
                        break;
            case 'p':   opt_given += opt_p;
                        strcpy(opt_p_arg,optarg);
                        opt_count++;
                        break;
            case 'v':   opt_given += opt_v;
                        opt_count++;
                        break;
            default:    opt_given = -1;
                        break;
        }
    }

    if ( opt_count > 1 ) {
        func_help();
        exit(EXIT_FAILURE);
    }

    return(opt_given);

}


