/* Implement list function (-l) which show all avaible spiekbriefjes.
 * 
 * Copyright (C) 2019  Peter van Gemert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

int func_list() {
    #include <dirent.h>

    struct dirent **namelist;
    int n;

    // Check contents of /usr/share/spiek
    char dirname[100] = "/usr/share/spiek/";

    printf("Location %s:\n",dirname);
    n = scandir(dirname,&namelist,0,alphasort);

    if (n<0) {
        perror("scandir");
    } else {
        int i=2; // Skip . and .. entries
        while (i < n) {
            printf("   %s\n",namelist[i]->d_name);
            free(namelist[i]);
            ++i;
        }
        free(namelist);
    }

    // Get home dir
    const char *homedir;

    if ((homedir = getenv("HOME")) == NULL) {
        homedir = getpwuid(getuid())->pw_dir;
    }

    // Check contents of ~/Dropbox/spiek
    char dirnamed[100] = "";
    snprintf(   dirnamed, \
                sizeof(dirnamed), \
                "%s%s", \
                homedir, \
                "/Dropbox/spiek");

    printf("\nLocation %s:\n",dirnamed);
    n = scandir(dirnamed,&namelist,0,alphasort);

    if (n<0) {
        perror("scandir");
    } else {
        int i=2; // Skip . and .. entries
        while (i < n) {
            printf("   %s\n",namelist[i]->d_name);
            free(namelist[i]);
            ++i;
        }
        free(namelist);
    }

    // Check contents of ~/.spiek
    char dirnameh[100] = "";
    snprintf(   dirnameh, \
                sizeof(dirnameh), \
                "%s%s", \
                homedir, \
                "/.spiek/");

    printf("\nLocation %s:\n",dirnameh);
    n = scandir(dirnameh,&namelist,0,alphasort);

    if (n<0) {
        perror("scandir");
    } else {
        int i=2; // Skip . and .. entries
        while (i < n) {
            printf("   %s\n",namelist[i]->d_name);
            free(namelist[i]);
            ++i;
        }
        free(namelist);
    }

    exit(0);
}

